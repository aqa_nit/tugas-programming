-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2019 at 01:24 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tgl_terbit` date NOT NULL,
  `gambar` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul`, `isi`, `tgl_terbit`, `gambar`) VALUES
(1, 'Kenapa Ibu Kota Baru Bukan di Jawa atau Sumatera?', ' Jakarta - Masih ada pihak yang belum puas dengan keputusan pemerintah memindahkan ibu kota negara (IKN) ke Kalimantan Timur (Kaltim). Kaltim dipilih langsung oleh Presiden Joko Widodo (Jokowi).\r\n\r\nKetua Pansus Pemindahan Ibu Kota Zainudin Amali bahkan menilai kajian Bappenas soal pemindahan ibu kota ke Kaltim belum lengkap.\r\n\r\nTerlepas dari itu, Ketua Tim Komunikasi Ibu kota Negara (IKN)/Sekretaris Menteri PPN/Bappenas Hirmawan Hariyoga Djojokusumo menegaskan bahwa rencana pemindahan ibu kota negara bukan sebuah rencana yang tiba-tiba. Sudah dilakukan kajian sejak 2017. ', '2019-10-01', ''),
(2, 'Sederet Kasus Korupsi yang Bikin Jajaran PUPR Dicokok KPK', 'Jakarta - Kasus korupsi terbongkar pada pembangunan jalan nasional di Kalimantan Timur. KPK mencokok Kepala Badan Jalan Nasional (BPJN) Wilayah XII Kalimantan Timur Refly Ruddy Tangkere. KPK menduga ada suap mengalir ke pembangunan jalan nasional di Kalimantan Timur. Kementerian PUPR sendiri mengaku terkejut, namun tetap akan kooperatif mendukung KPK menyelesaikan kasus korupsi jajarannya. Jauh sebelum itu, Kementerian PUPR sendiri memang sering terganjal kasus korupsi. Sepanjang 2019 KPK terus mengembangkan penyelidikan pada kasus korupsi SPAM. Berikut ini, beberapa kasus korupsi yang pernah menyandung PUPR.', '2019-10-17', '');

-- --------------------------------------------------------

--
-- Table structure for table `seminar`
--

CREATE TABLE `seminar` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `usia` int(11) NOT NULL,
  `asal` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `jenis_instansi` varchar(50) NOT NULL,
  `nama_instansi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seminar`
--

INSERT INTO `seminar` (`id`, `nama`, `email`, `no_telp`, `tempat_lahir`, `tgl_lahir`, `usia`, `asal`, `status`, `jenis_instansi`, `nama_instansi`) VALUES
(1, 'admin', 'admin@gmail.com', '0987654321', 'Bandung', '1998-08-17', 21, 'Bandung', 'Menikah', 'Badan Khusus', 'FBI'),
(4, 'Ahmad Qonit Abdullah', 'santrisiber.qonit@gmail.com', '082120756715', 'Bandung', '1999-05-28', 20, 'Bandung', 'Belum Menikah', 'Swasta', 'PSB');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `asal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `asal`) VALUES
(2, 'admin', '$2y$10$NmwCxjhXovSg6HCo9552SOsm0KYH.yPbWl7RD4IUqhCSHIZSzqEiO', 'admin@gmail.com', 'Indonesia');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seminar`
--
ALTER TABLE `seminar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `seminar`
--
ALTER TABLE `seminar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
